# Authentication

1. At the address http://auth-starter.noinputsignal.com/api/, you will find an API with a database of jokes.
2. The system allows registering a new user who, after logging in, can add their joke.
3. Create registration and login forms.
4. Log in the user and then prepare a form for adding jokes for them.