# Hooks #

1. Examine the component in the pages/usestate file. Use state to define the button's color. Add an event that changes the button's color after clicking.
2. In the file, you'll find a set of questions (pages/useeffect). Reset error information each time the user moves to the next question.
3. In the pages/useref file, there is a small component. Add animation to the red div. Move it down (300px) and to the right (300px). Change its color to blue. Use the animate function from jQuery (https://api.jquery.com/animate/).
4. In the pages/usememo file, you'll find a component with two functionalities: changing the color and calculating factorial. Ensure that the factorial is calculated only when necessary.
5. Examine the component in pages/callback. Consider why the callback is triggered every second and try to prevent it.
6. Check the code in pages/usereducer. Eliminate errors in the equation.
7. In the pages/forwardref file, use a reference in the parent to focus the input in the child component.
8. In the usedeferredvalue.tsx file, optimize the script's performance using the useDeferredValue hook. Try using the useTransition hook as well.