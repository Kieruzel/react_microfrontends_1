import React, { useEffect, useState } from "react";

const capitals = [
  {
    country: "France",
    capital: "Paris",
  },
  {
    country: "Poland",
    capital: "Warsaw",
  },
  {
    country: "Italy",
    capital: "Rome",
  },
];

const UseState: React.FC = (): JSX.Element => {
  // const [correctAnswer, setCorrectAnswer] = useState(null);
  // const [currentQuestion, setCurrentQuestion] = useState(0);

  const [state, setState] = useState({
    correctAnswer: null,
    currentQuestion: 0,
  });

  const [input, setInput] = useState("");

  return (
    <div>
      <h1>The capital of {capitals[state.currentQuestion].country} is ?</h1>
      <input value={input} onChange={(e) => setInput(e.currentTarget.value)} />
      <button
        onClick={() => {
          setState((prev) => ({
            ...prev,
            correctAnswer: input === capitals[state.currentQuestion].capital,
          }));
        }}
      >
        Check the answer
      </button>
      <button
        onClick={() =>
          setState((prev) => ({
            ...prev,
            correctAnswer: null,
            currentQuestion: (prev.currentQuestion + 1) % 3,
          }))
        }
      >
        Next question
      </button>

      {state.correctAnswer === true && "Correct Answer"}
      {state.correctAnswer === false && "Wrong Answer"}
    </div>
  );
};

export default UseState;
