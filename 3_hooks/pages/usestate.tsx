import React, { useState } from "react";

function UseState() {
  let [color, setColor] = useState("pink");
  const [counter, setCounter] = useState(0);


  return (
    <button
      style={{ padding: "30px", fontSize: "26px", backgroundColor: color }}
      onClick={() => {
        console.log("I've cliked");
        const randomColor = Math.floor(Math.random() * 16777215).toString(16);

        // color = "#" + randomColor;

        setColor("#" + randomColor);
        setCounter(counter + 1);
      }}
    >
      Change color {counter}
    </button>
  );
};

export default UseState;
