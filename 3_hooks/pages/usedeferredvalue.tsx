import React, {
  useDeferredValue,
  useEffect,
  useState,
  useTransition,
} from "react";
import times from "lodash/times";

type Props = {};

const usedeferredvalue: React.FC<Props> = ({}): JSX.Element => {
  const [input, setInput] = useState<number | "">(0);
  const [deferredInput, setDeferredInput] = useState<number | "">(0);
  // const deferredInput = useDeferredValue(input)

  const [isPending, startTransition] = useTransition();

  useEffect(() => {
    startTransition(() => {
      setDeferredInput(input);
    });
  }, [input]);

  const generateElements = (number) => {
    return times(number, (num) => {
      return <li key={num}>{num}</li>;
    });
  };

  return (
    <section>
      <input
        type="number"
        value={input}
        onChange={(e) =>
          setInput(
            e.currentTarget.value === "" ? "" : Number(e.currentTarget.value)
          )
        }
      ></input>
      {
        isPending && <h1>Loading...</h1>
      }
      <ul>{generateElements(deferredInput)}</ul>
    </section>
  );
};

export default usedeferredvalue;
