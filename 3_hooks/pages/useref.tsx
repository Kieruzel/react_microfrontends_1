import React, { useEffect, useRef } from "react";
import $ from 'jquery';

const style : CSSStyleSheet = {
  width: "100px",
  height: "100px",
  backgroundColor: "red",
  position: "relative",
};

const Useref: React.FC = (): JSX.Element => {
  const [counter, setCounter] = React.useState(0);
  const ref = useRef(null);
  const ref2 = useRef(0);
  let number = 0;

  useEffect(() => {

    console.log(ref.current);

    $(ref.current).animate({
      left: '300px',
      top: '300px',
      background: "blue"
    }, 3000)

    // ref.current.style.backgroundColor = "blue";

    setInterval(() => {
      ref2.current += 1;
      number += 1;
      setCounter(prev => prev + 1); 
    }, 2000);

  }, [])

  console.log("__", "Ref2", ref2.current, "Number", number);




  return <div ref={ref} style={style}></div>;
};

export default Useref;
