import { memo, useEffect, useRef, useState } from "react";

const Child = memo(
  ({ time }: { time: string }) => {
    const ref = useRef(null);

    useEffect(() => {
      console.log("Child rendered");
    });

    return (
      <div>
        <h1>Child {time}</h1>
      </div>
    );
  },
  (prev, next) => {
    console.log("Child memoized");
    return prev.time === next.time;
  }
);

const IndexPage = () => {
  const [date, setDate] = useState(new Date());
  const [tenSeconds, setTenSeconds] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => setDate(new Date()), 1000);
    const interval2 = setInterval(() => setTenSeconds(new Date()), 10000);

    return () => {
      clearInterval(interval2);
      clearInterval(interval);
    };
  }, []);

  return (
    <div>
      <h1>Index Page</h1>
      <h2>{date.toLocaleTimeString()}</h2>
      <Child time={tenSeconds.toLocaleTimeString()} />
    </div>
  );
};

export default IndexPage;
