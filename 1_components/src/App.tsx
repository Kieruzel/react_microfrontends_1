import React from "react";
import db from "./db.json";
import "./App.css";

// "hight": "168",
// "weight": "52",
// "birth": "1994",
// "spike": "264",
// "block": "258",
// "nationality": "ROU"

type PlayerProps = {
  img: string;
  name: string;
  position: string;
  weight: string;
  hight: string;
  birth: string;
  spike: string;
  block: string;
  nationality: string;
};

const Player = ({
  img,
  name,
  position,
  weight,
  hight,
  birth,
  spike,
  block,
  nationality,
}: PlayerProps) => {
  return (
    <tr className="rlvI">
      <td align="center">
        <a id="" href="" target="_self">
          <div className="img_box">
            <img
              id=""
              src={img}
              style={{ borderWidth: 0, borderStyle: "None" }}
            />
          </div>
        </a>
        <div style={{ position: "relative" }}></div>
      </td>
      <td align="center" />
      <td>
        <a id="" href="">
          {name}
        </a>
      </td>
      <td align="center">{position}</td>
      <td align="center">{hight}</td>
      <td align="center">{weight}</td>
      <td align="center">{birth}</td>
      <td align="center">{block}</td>
      <td align="center">{spike}</td>
      <td align="center">{nationality}</td>
    </tr>
  );
};

function App() {
  const [x, setX] = React.useState(false);



  return (
    <div className="container">
      <table cellSpacing={0} style={{ width: "100%" }}>
        <thead>
          <tr className="rlvHeader">
            <th> </th>
            <th style={{ textAlign: "center" }}> # </th>
            <th> Name </th>
            <th style={{ textAlign: "center" }}> Position </th>
            <th style={{ textAlign: "center" }}> Height </th>
            <th style={{ textAlign: "center" }}> Weight </th>
            <th style={{ textAlign: "center" }}> Birth Year </th>
            <th style={{ textAlign: "center" }}> Spike reach </th>
            <th style={{ textAlign: "center" }}> Block reach </th>
            <th style={{ textAlign: "center" }}> Nationality </th>
          </tr>
        </thead>
        <tbody>


          {
            x ? <input /> : <input />
          }


          {db.map((player: PlayerProps, index: number) => {
            const p = (
              <Player
                key={player.name}
                img={player.img}
                name={player.name}
                position={player.position}
                hight={player.hight}
                weight={player.weight}
                birth={player.birth}
                spike={player.spike}
                block={player.block}
                nationality={player.nationality}
              />
            );

            console.log("Element", p)

            return p;
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
