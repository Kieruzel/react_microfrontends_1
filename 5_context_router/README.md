# Real Estate

1. In the feed.json file, you will find a database of a real estate portal (run it as a REST API using json-server).
2. Load all the real estate properties onto the homepage - use useSWR or React Query.
3. Create subpages for each individual real estate property.
4. Create a context that will hold information about user choices.
5. Create a form for submitting a "lead."
6. Try to load components in Lazy mode.
7. Optimize the context using react-tracked.
8. Implement product filtering by categories on the homepage. Optimize the filtering using useTransition.