# Bank simulation

"Let's play a simulation of a bank. Customers can take loans and make deposits. Assume that the interest rate on deposits is 2% every 5 seconds and the interest rate on loans is 5% every 5 seconds.

1. Create reducers for the bank and the customer.
2. Assume that a customer has one loan and can increase it. Similarly with deposits.
3. Calculate everything. Every 5 seconds, the account balance of the bank's customers must be updated.
4. Calculate the entire bank's balance."